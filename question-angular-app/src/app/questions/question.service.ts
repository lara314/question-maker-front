import {HttpClient, HttpParams} from '@angular/common/http';
import {Question} from './question.model';
import {Injectable} from '@angular/core';

@Injectable({providedIn: 'root'})
export class QuestionService {

  private questions: Question[] = [];

  constructor(private http: HttpClient) {
  }


  fetchQuestions(pageIndex: any, pageSize: any, sortActive?: any, sortDirection?: any) {

    const sort =  (sortActive || 'id') + ',' + (sortDirection || 'ASC');
    const params = new HttpParams().set('page', pageIndex).set('size', pageSize).set('sort', sort);
    return this.http.get('http://localhost:8080/api/questions', {
      params,
      observe: 'response'
   });
  }

  // tslint:disable-next-line:typedef
  getQuestion(index: number) {
    // return this.questions[index];
  return this.http.get('http://localhost:8080/api/questions/' + index);
  }

  // tslint:disable-next-line:typedef
  addQuestion(question: Question) {
    return this.http.post('http://localhost:8080/api/questions', question);
  }


  updateQuestion(id: number, question: Question) {
    console.log('Ovo je question!');
    console.log(question);
    return this.http.put('http://localhost:8080/api/questions/' + id, question);
  }

  deleteQuestion(id: number) {
    return this.http.delete('http://localhost:8080/api/questions/' + id);
  }
}
