import { Component, OnInit } from '@angular/core';
import {FormArray, FormControl, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {QuestionService} from "../question.service";
import {forEachComment} from "tslint";

@Component({
  selector: 'app-question-new',
  templateUrl: './question-new.component.html',
  styleUrls: ['./question-new.component.css']
})
export class QuestionNewComponent implements OnInit {

  questionForm: FormGroup;
  // ISPOBAVANJE ZA CUSTOM VALIDATOR
  atLeastOneCheckedTrue = false;

  constructor(
    private route: ActivatedRoute,
    private questionService: QuestionService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.questionForm = new FormGroup({
      questionText: new FormControl('', Validators.required),
      points: new FormControl(0, Validators.required),
      questionType: new FormControl('RADIO_BUTTON', Validators.required),
      answers: new FormArray([new FormGroup({
        // DODAJEM QUESTION_ID
        answer: new FormControl(''),
        correct: new FormControl(''),
        questionId: new FormControl('')
      })])
    }, this.trueAnswerChecker);
  }

  onCancel() {
    this.router.navigate(['../'], { relativeTo: this.route });
  }

  onSubmit() {
    this.questionService.addQuestion(this.questionForm.value).subscribe(
      question => {
        alert('You added a question!');
        this.router.navigate(['../'], { relativeTo: this.route });
      }
    );
    this.onCancel();
  }

  getAnswerControls(): any {
    return (this.questionForm.get('answers') as FormArray).controls;
  }

  // onAddAnswer() {
  //   (this.questionForm.get('answers') as FormArray).push(new FormControl(null));
  // }
  onAddAnswer() {
    (this.questionForm.get('answers') as FormArray).push(new FormGroup({
      answer: new FormControl(''),
      correct: new FormControl(false),
      questionId: new FormControl('')  // DA LI TREBA QUESTION_ID
    }));
    // console.log('Sta je ovoooo');
    // console.log((this.questionForm.get('answers') as FormArray));
    // console.log('and this');

    let answer;
    // tslint:disable-next-line:forin
    for (answer in this.questionForm.get('answers') as FormArray) {
       // console.log(answer);
     }

    // for (let i = 0; i < this.questionForm.get('answers') as FormArray).controls.length ; i++ ) {
    //   if (this.questionForm.get('answers') as FormArray).controls.indexOf(i)) {
    //
    //   }
    // }
  }

  trueAnswerChecker(form: FormGroup): {[s: string]: boolean} {

    return (form.get('answers') as FormArray).controls.filter(
      answer => answer.value.correct
    ).length == 0 ? {'atLeastOneCheckedTrue' : false} : null

  }

  trueAnswerCheckerArray(array: FormArray): {[s: string]: boolean} {

    return null;
  }
}
