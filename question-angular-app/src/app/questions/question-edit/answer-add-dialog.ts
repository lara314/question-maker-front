import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog} from '@angular/material/dialog';

// /**
//  * @title Dialog with header, scrollable content and actions
//  */
// @Component({
//   selector: 'dialog-content-example',
//   templateUrl: 'dialog-content-example.html',
// })
// // tslint:disable-next-line:component-class-suffix
// export class DialogContentExample {
//   constructor(public dialog: MatDialog) {}
//
//   openDialog() {
//     const dialogRef = this.dialog.open(AnswerAddDialog);
//
//     dialogRef.afterClosed().subscribe(result => {
//       console.log(`Dialog result: ${result}`);
//       console.log('Ovo je rezultat: ' + result);
//     });
//   }
// }

@Component({
  selector: 'app-answer-add-dialog',
  templateUrl: 'answer-add-dialog.html',
})
export class AnswerAddDialog {

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) { }
  
}
