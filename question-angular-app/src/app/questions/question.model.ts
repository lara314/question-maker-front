import {Answer} from './answer/answer.model';

export class Question {
  public id: number;
  public questionText: string;
  public points: number;
  public questionType: string;
  public answers: Answer[];
  public selected: boolean;

  constructor(id: number, questionText: string, points: number, questionType: string, answers: Answer[]) {
    this.id = id;
    this.questionText = questionText;
    this.points = points;
    this.questionType = questionType;
    this.answers = answers;
  }
}
