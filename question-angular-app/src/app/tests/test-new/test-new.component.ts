import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {Test} from "../test.model";
import {QuestionService} from '../../questions/question.service';
import {Question} from "../../questions/question.model";
import {MatPaginator, PageEvent} from "@angular/material/paginator";
import {TestsService} from "../tests.service";

@Component({
  selector: 'app-test-new',
  templateUrl: './test-new.component.html',
  styleUrls: ['./test-new.component.css']
})
export class TestNewComponent implements OnInit, AfterViewInit {

  // @ts-ignore
  test: Test = {
    id: 0,
    title: 'how about now',
    questions: []
  };

  questions: any;
  length: any;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  // private dataSource: MatTableDataSource<Question>;

  constructor(private questionService: QuestionService,
              private testsService: TestsService) {
  }

  reactOnChange(question: Question) {
    if (this.test.questions.includes(question)) {
      this.test.questions = this.test.questions.filter(q => q.id !== question.id);
      return;
    }
    this.test.questions.push(question);
  }

  ngOnInit(): void {
    // this.fetch(true);
    // PROBAJ BEZ MERGE
    // merge(this.paginator.pageIndex, this.paginator.pageSize).subscribe(() =>
    //   this.fetch(false));
  }

  ngAfterViewInit(): void {
    this.fetch(true);
  }

  fetch(setPaginator: boolean) {
    this.questionService.fetchQuestions(this.paginator.pageIndex || 0, this.paginator.pageSize || 10).subscribe(
      (response) => {
        this.questions = response.body;
        this.checkSelectedQuestions();
        this.length = +response.headers.get('X-total-elements');
      }
    );
  }

  private checkSelectedQuestions(): void {
    this.test.questions.forEach(selected => {
      this.questions.forEach(question => {
        if (selected.id === question.id) {
          question.selected = true;
        }
      });
    });
    console.log(this.questions);
  }

  sendRequest(event: PageEvent) {
    this.questionService.fetchQuestions(event.pageIndex || 0, event.pageSize || 10).subscribe(
      (response) => {
        this.questions = response.body;
        this.checkSelectedQuestions();
        this.length = +response.headers.get('X-total-elements');
      }
    );
  }

  onCreateTest() {
    console.log(this.test);
    this.testsService.addTest(this.test).subscribe(
      test => alert('You created a test!')
    );
  }

}
