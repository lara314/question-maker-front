import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Test} from './test.model';

// @ts-ignore
@Injectable({providedIn: 'root'})
export class TestsService {

  private tests: Test[] = [];

  constructor(private http: HttpClient) {
  }

  getTests() {
    return this.http.get('http://localhost:8080/api/tests');
  }

  addTest(test: Test) {
    return this.http.post('http://localhost:8080/api/tests', test);
  }

  // fetchQuestions() {
  //   return this.http.get('http://localhost:8080/api/questions');
  // }
  //
  // // tslint:disable-next-line:typedef
  // getQuestion(index: number) {
  //   // return this.questions[index];
  //   return this.http.get('http://localhost:8080/api/questions/' + index);
  // }
  //
  // // tslint:disable-next-line:typedef
  // addQuestion(question: Question) {
  //   return this.http.post('http://localhost:8080/api/questions', question);
  // }
  //
  //
  // updateQuestion(id: number, question: Question) {
  //   console.log('Ovo je question!');
  //   console.log(question);
  //   return this.http.put('http://localhost:8080/api/questions/' + id, question);
  // }
  //
  // deleteQuestion(id: number) {
  //   return this.http.delete('http://localhost:8080/api/questions/' + id);
  // }
}
