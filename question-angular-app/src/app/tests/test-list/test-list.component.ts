import { Component, OnInit } from '@angular/core';
import {TestsService} from "../tests.service";
import {Test} from "../test.model";

@Component({
  selector: 'app-test-list',
  templateUrl: './test-list.component.html',
  styleUrls: ['./test-list.component.css']
})
export class TestListComponent implements OnInit {

  tests: Test[];

  constructor(
    private testsService: TestsService
  ) { }

  ngOnInit(): void {
    this.testsService.getTests().subscribe(
      (tests: Test[]) => this.tests = tests
    );
  }

}
